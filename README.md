# IMDB-SII
Mini projekt dla SII - search bar IMDB.  

## Info
1) npm install -g @angular/cli  
2) npm install  
3) ng serve  
4) http://localhost:4200/  

## Uwagi
1) IMDB nie udostępnia publicznego API, użyłem więc OMDB.  
2) Jeśli zadanie tego wymaga, to oczywiście mogę dopisać testy jednostkowe.  

