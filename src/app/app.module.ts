import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import {OMDBAPIService} from "./omdb/omdb-api.service";
import {OMDBSearchResultsParser} from "./omdb/search/omdb-search-results-parser";
import {OMDBService} from "./omdb/omdb.service";
import { OMDBSearchBarComponent } from './omdb/search/search-bar/omdb-search-bar.component';
import {IMDBService} from "./imdb/imdb.service";
import { OMDBSearchResultTileComponent } from './omdb/search/result-tile/omdb-search-result-tile.component';
import {OMDBConstants} from "./omdb/omdb-constants";
import {TruncatePipe} from "./pipes/truncate.pipe";

@NgModule({
  declarations: [
    AppComponent,
    OMDBSearchBarComponent,
    OMDBSearchResultTileComponent,
    TruncatePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot()
  ],
  providers: [
    OMDBAPIService,
    OMDBSearchResultsParser,
    OMDBService,
    IMDBService,
    OMDBConstants
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
