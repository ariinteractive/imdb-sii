import { Injectable } from '@angular/core';
import {OMDBAPIService} from "./omdb-api.service";
import {Observable} from "rxjs";
import {OMDBSearchResult} from "./search/omdb-search-result";
import {OMDBSearchResultsParser} from "./search/omdb-search-results-parser";

@Injectable()
export class OMDBService {

  constructor(
    private omdbAPI:OMDBAPIService,
    private omdbSearchResultsParser:OMDBSearchResultsParser
  ) {}

  public searchFor(searchQuery:string):Observable<OMDBSearchResult[]>{
    return this.omdbAPI
      .searchFor(searchQuery)
      .map((response) => this.omdbSearchResultsParser.createFromResponse(response));
  }
}
