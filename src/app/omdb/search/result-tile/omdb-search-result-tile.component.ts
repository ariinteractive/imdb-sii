import {Component, Input, OnInit} from '@angular/core';
import {OMDBSearchResult} from "../omdb-search-result";
import {OMDBConstants} from "../../omdb-constants";

@Component({
  selector: 'app-omdbsearch-result-tile',
  templateUrl: 'omdb-search-result-tile.component.html',
  styleUrls: ['omdb-search-result-tile.component.css']
})

export class OMDBSearchResultTileComponent implements OnInit{

  @Input() omdbSearchResult:OMDBSearchResult;
  private posterURL;

  constructor(private omdbConstants:OMDBConstants){}

  ngOnInit(): void {
    if (this.omdbSearchResult) {
      this.posterURL = this.omdbSearchResult.hasPoster() ?
        this.omdbSearchResult.posterURL : this.omdbConstants.DEFAULT_POSTER_URL;
    }
    else{
      this.invalidPosterHandler();
    }
  }

  private invalidPosterHandler():void{
    this.posterURL = this.omdbConstants.DEFAULT_POSTER_URL;
  }
}
