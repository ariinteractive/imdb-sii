export class OMDBSearchResult {

  constructor(
    public readonly title:string,
    public readonly year:string,
    public readonly imdbID:string,
    public readonly posterURL:string,
    public readonly type:string
  ){}

  public hasPoster():boolean{
    return this.posterURL.length > 0;
  }
}
