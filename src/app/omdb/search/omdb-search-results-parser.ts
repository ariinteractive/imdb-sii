import {Injectable} from "@angular/core";
import {OMDBSearchResult} from "./omdb-search-result";
import {OMDBConstants} from "../omdb-constants";

@Injectable()
export class OMDBSearchResultsParser {
  constructor(private omdbConstants:OMDBConstants){}

  public createFromResponse(response:any):OMDBSearchResult[]{
    return this.isResponsePositive(response) ? this.getSearchResultsFromResponse(response) : [];
  }

  private isResponsePositive(response:any){
    return response.Response == this.omdbConstants.RESPONSE_POSITIVE;
  }

  private getSearchResultsFromResponse(response:any):OMDBSearchResult[]{
    let results:OMDBSearchResult[] = [];
    for(let resultResponse of response.Search){
      results.push(this.createSearchResult(resultResponse));
    }
    return results;
  }

  private createSearchResult(searchResponse:any):OMDBSearchResult{
    return new OMDBSearchResult(
      searchResponse.Title,
      searchResponse.Year,
      searchResponse.imdbID,
      searchResponse.Poster == this.omdbConstants.NOT_AVAILABLE ? "" : searchResponse.Poster,
      searchResponse.Type
    );
  }
}
