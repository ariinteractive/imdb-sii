import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {NgbTypeaheadSelectItemEvent} from "@ng-bootstrap/ng-bootstrap";
import {OMDBService} from "../../omdb.service";
import {IMDBService} from "../../../imdb/imdb.service";
import {OMDBSearchResult} from "../omdb-search-result";

@Component({
  selector: 'app-omdb-search-bar',
  templateUrl: 'omdb-search-bar.component.html',
  styleUrls: ['omdb-search-bar.component.css']
})

export class OMDBSearchBarComponent {

  private readonly DEBOUNCE_MILLISECONDS:number = 100;
  private typedText:string = "";

  constructor(
    private omdb:OMDBService,
    private imdb:IMDBService
  ){}

  public handleElementClick(selected:NgbTypeaheadSelectItemEvent):void{
    this.typedText = selected.item.title;
    this.imdb.navigateToMoviePage(selected.item.imdbID);
  }

  public handleSubmit():void{
    this.imdb.navigateToFindPage(this.typedText);
  }

  private formatter(x:OMDBSearchResult):string{
    return x.title;
  }

  private search = (searchQuery$:Observable<string>) =>
    searchQuery$
      .do(text => this.typedText = text)
      .debounceTime(this.DEBOUNCE_MILLISECONDS)
      .distinctUntilChanged()
      .switchMap(text =>
        this.omdb.searchFor(text)
          .catch((error:any) => [])
      );

}
