import { Injectable } from '@angular/core';
import {Http, Response, URLSearchParams} from "@angular/http";
import {Observable} from "rxjs";
import {OMDBConstants} from "./omdb-constants";

@Injectable()
export class OMDBAPIService {

  constructor(
    private http:Http,
    private omdbConstants:OMDBConstants
  ){}

  public searchFor(searchQuery:string):Observable<any>{
    let params = new URLSearchParams();
    params.set('s', searchQuery.trim());
    return this.http
      .get(this.omdbConstants.BASE_URL, {search: params})
      .map((response:Response) => response.json())
      .catch((error:any) => Observable.throw(this.omdbConstants.API_SEARCH_ERROR));
  }
}
