import { Injectable } from '@angular/core';

@Injectable()
export class OMDBConstants {
  public readonly DEFAULT_POSTER_URL:string = "../../assets/default_poster.png";
  public readonly BASE_URL:string = "http://www.omdbapi.com";
  public readonly NOT_AVAILABLE:string = "N/A";
  public readonly RESPONSE_POSITIVE:string = "True";
  public readonly RESPONSE_NEGATIVE:string = "False";
  public readonly API_SEARCH_ERROR:string = "OMDB API Search Error";
}
