import { Injectable } from '@angular/core';

@Injectable()
export class IMDBService {

  public navigateToMoviePage(movieID:String):void{
    window.location.href = "http://www.imdb.com/title/" + movieID;
  }

  public navigateToFindPage(searchQuery:String):void{
    window.location.href = "http://www.imdb.com/find?ref_=nv_sr_fn&q=" + searchQuery + "&s=all";
  }
}
